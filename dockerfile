FROM node:19-alpine3.16

LABEL description="a simple docker app"

WORKDIR /usr/react-app

EXPOSE 4200

RUN npm i -g serve

COPY build build/

ENTRYPOINT ["serve"]
CMD ["-s", "./build", "-l", "4200" ]


